import React, { useReducer } from 'react';
import './App.css';
import ComponentsA from './components/ComponentsA';
import ComponentsD from './components/ComponentsD';
import ComponentsF from './components/ComponentsF';
// import CounterThree from './components/CounterThree';
// import CounterOne from './components/CounterOne';
// import CounterTwo from './components/CounterTwo';

export const CountContext = React.createContext()

const initialState = 0
const reducer = (state, action) => {
  switch (action) {
    case 'increment':
      return state + 1
    case 'decrement':
      return state - 1
    case 'reset':
      return initialState
    default:
      return state
  }
}

function App() {
  const [count, dispatch] = useReducer(reducer, initialState)
  return (
    <CountContext.Provider
      value={{ countState: count, countDispatch: dispatch }}
    >
      <div className="App">
        Count - {count}
        <ComponentsA />
        <ComponentsD />
        <ComponentsF />
        {/* <CounterThree /> */}
        {/* <CounterTwo /> */}
        {/* <CounterOne /> */}
      </div>
    </CountContext.Provider>
  );
}

export default App;
